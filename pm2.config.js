const { name } = require('./package')

const config = {
  apps: [
    {
      name: `${name}`,
      script: '.next/dist/index.js',
      watch: false,
      autorestart: true,
      log_date_format: 'YYYY-MM-DD HH:mm Z',
      instances: 1,
      exec_mode: 'cluster',
      instance_var: 'INSTANCE_ID',
      node_args: '--nouse_idle_notification --max-old-space-size=2048',
      log_file: `logs/${name}/pm2/log/pm2.log`,
      out_file: `logs/${name}/pm2/out/out.log`,
      err_file: `logs/${name}/pm2/err/err.log`,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
}

module.exports = config
