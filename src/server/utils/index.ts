/* eslint-disable no-bitwise */
/* eslint-disable no-return-assign */
import { Context } from 'koa'

export const createGuide = (t = +new Date()) => {
  const stringRule = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
  return stringRule.replace(/[xy]/g, e => {
    let r = 0
    let n = 0
    return (
      (r = (t + 16 * Math.random()) % 16 | 0),
      (t = Math.floor(t / 16)),
      (n = e === 'x' ? r : (3 & r) | 8),
      n.toString(16)
    )
  })
}

export const getIp = (ctx: Context) => {
  const realIp = typeof ctx.header['x-real-ip'] !== 'undefined' ? ctx.header['x-real-ip'] : ctx.ip
  const ip =
    typeof ctx.header['x-forwarded-for'] !== 'undefined' && ctx.header['x-forwarded-for'].length > 0
      ? ctx.header['x-forwarded-for'].split(',')[0].replace(' ', '')
      : realIp
  return ip
}

export const nodeParams = (ctx: Context) => ({
  ip: getIp(ctx),
  cookie: ctx.headers.cookie || '',
  'Content-Type': 'application/json',
  'User-Agent': ctx.request.header['user-agent'] || '',
})
