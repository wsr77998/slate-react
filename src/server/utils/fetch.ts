/* eslint-disable no-underscore-dangle */
import { Response } from 'koa'
import axios from 'axios'

export type Options = {
  url: string
  method: string
  data: any
  params: any
  timeout?: number
  headers?: {
    [key: string]: any
  }
}

type ReturnRes = {
  isError: boolean
  resp: Response | Error
}

const fetch = (fetchOption: Options): Promise<ReturnRes & any> => {
  const { method, params, data, url, headers = {}, timeout } = fetchOption
  let _url = url

  const scheduleApi = '/api/schedule_lol'
  if (_url.indexOf(scheduleApi) === 0) {
    _url = `${process.env.SCHEDULE_LOL_API_HOST}${url.slice(scheduleApi.length)}`
  } else if (_url.indexOf('/api') === 0) {
    _url = `${process.env.API_HOST}${url.slice(4)}`
  }

  const options: Options = {
    url: _url,
    method,
    params,
    data: method === 'GET' ? null : data,
    headers,
    timeout: timeout || parseInt(process.env.REQUEST_TIMEOUT, 10),
  } as Options

  return axios(options)
    .then((res: any) => ({
      resp: res,
      isError: false,
    }))
    .catch((err: Error) => ({
      isError: true,
      resp: err,
    }))
}

export default fetch
