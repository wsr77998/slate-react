const Router = require('koa-router')

const router = new Router()

router.get('/timestamp', ctx => {
  ctx.body = {
    data: Date.now(),
  }
})

module.exports = router
