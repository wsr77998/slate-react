/* eslint-disable no-underscore-dangle */
/* eslint-disable prefer-destructuring */
import { Context, Next } from 'koa'
import fetch, { Options } from '../utils/fetch'
import { nodeParams } from '../utils'

module.exports = () => async (ctx: Context, next: Next) => {
  if (ctx.request.method === 'OPTIONS') {
    ctx.status = 204
    await next()
  }
  if (ctx.req.url.indexOf('/api') === 0) {
    const apiStart = new Date().getTime()
    const headers = nodeParams(ctx)
    let _url = ctx.req.url
    _url = _url.split('?')[0]

    const options: Options = {
      url: _url,
      method: ctx.method,
      data: ctx.request.body,
      params: ctx.request.query,
      headers,
    }

    let result
    try {
      result = await fetch(options)
      const apiEnd = new Date().getTime()
      if (apiEnd - apiStart >= 1000) {
        logger.warn('API Request Time > 1s', apiEnd - apiStart, ctx.req.url)
      }
      if (result.isError) {
        if (result.resp && result.resp.response && result.resp.response.status) {
          ctx.status = result.resp.response.status
        } else {
          ctx.status = 500
        }
        try {
          ctx.type = result.resp.response.headers['content-type'].includes('html') ? 'html' : ''
          ctx.body = result.resp.response.data
        } catch (error) {
          ctx.type = 'html'
          ctx.body = error
        }
      } else {
        ctx.status = result.resp.status
        ctx.body = result.resp.data
      }
    } catch (e) {
      logger.error('apiMiddleware:', e)
      ctx.status = 500
      result = {
        code: 500,
        message: 'System error.',
        success: false,
      }
      ctx.body = result
    }
  } else {
    await next()
  }
}
