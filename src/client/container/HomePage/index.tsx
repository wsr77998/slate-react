import React, { useMemo, useState } from 'react'
import { createEditor } from 'slate'
import { Slate, Editable, withReact } from 'slate-react'

import './style.less'

export default () => {
  const editor = useMemo(() => withReact(createEditor()), [])
  // Add the initial value when setting up our state.
  const [value, setValue] = useState([
    {
      type: 'paragraph',
      children: [{ text: 'A line of text in a paragraph.' }],
    },
  ])

  return (
    <section className="homePage">
      <Slate editor={editor} value={value} onChange={(value: any) => setValue(value)}>
        <Editable />
      </Slate>
    </section>
  )
}
