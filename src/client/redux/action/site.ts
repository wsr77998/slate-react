import fetch, { RequestParams } from '@/utils/fetch'
import * as types from '../types'

export const getServerTimeStamp = (params: RequestParams) => dispatch =>
  fetch(params).then((res: any) =>
    dispatch({
      type: types.SET_SERVER_TIME,
      payload: {
        data: res.data,
      },
    })
  )

export const addCount = () => ({
  type: types.ADD_COUNT,
})

export const setCount = (data: number) => ({
  type: types.SET_COUNT,
  payload: {
    data,
  },
})
