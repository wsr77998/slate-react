import * as types from '../types'

const initialState = {
  count: 0,
  _count: 0,
  time: 0,
}

export default (state = { ...initialState }, action) => {
  switch (action.type) {
    case types.SET_SERVER_TIME: {
      return {
        ...state,
        time: action.payload.data,
      }
    }

    case types.ADD_COUNT: {
      return {
        ...state,
        count: state.count + 1,
        _count: state._count + 1,
      }
    }

    case types.SET_COUNT: {
      return {
        ...state,
        count: action.payload.data,
      }
    }

    default: {
      return state
    }
  }
}
