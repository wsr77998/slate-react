/* eslint-disable no-underscore-dangle */
/* eslint-disable prefer-destructuring */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable import/no-cycle */
import * as Cookies from 'js-cookie'

export { default as fetch } from './fetch'

export const noop = () => {}

export const setCookie = (key: string, value: any, attr: object = {}) => {
  Cookies.set(key, value, {
    ...attr,
    domain: `${process.env.COOKIE_DOMAIN}`,
  })
}

export const getCookie = (cookieName: string) => Cookies.get(cookieName)

export const sleep = (time = 1) =>
  new Promise(reslove => {
    setTimeout(reslove, time * 1000)
  })

export const parseNum = (num: number) => `${num >= 10 ? num : `0${num}`}`

export const getParam = (search: string) => {
  const _$u = search.slice(1)
  let $u = []
  const r = {}
  if (_$u) {
    $u = _$u.split('&')
  } else {
    return r
  }
  $u.forEach(el => {
    const $n = el.split('=')
    r[$n[0]] = $n[1]
  })
  return r
}

export const ERROR_IMG_SRC =
  'https://resource.vpesports.com/resource/game/csgo/team/d8290397364eef4a75e13eb67eaf27af.svg'

export const setErrorImg = e => {
  if (e && e.target) {
    e.target.src = ERROR_IMG_SRC
  }
}
