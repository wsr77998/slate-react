import React from 'react'
import NextSeo from 'next-seo'
import HomePage from '@/container/HomePage'
import getOrCreateStore from '@/lib/with-redux-store'
import { getServerTimeStamp } from '@/redux/action/site'

export default class extends React.Component<any, any> {
  // 服务端渲染时请求接口demo 一定要判断req 是否存在 然后将数据通过redux传递给前端
  static async getInitialProps({ req }) {
    const reduxStore = getOrCreateStore()
    if (req) {
      await reduxStore.dispatch(
        getServerTimeStamp({
          url: '/timestamp',
          host: true,
        })
      )
    }
    return {
      initialReduxState: reduxStore.getState(),
    }
  }

  render() {
    return (
      <>
        <NextSeo
          config={{
            title: 'Index Page Title',
            description: 'Index Page description',
          }}
        />
        <HomePage />
      </>
    )
  }
}
