/* eslint-disable no-underscore-dangle */
import App, { Container } from 'next/app'
import React from 'react'
import Head from 'next/head'
import getOrCreateStore from '@/lib/with-redux-store'
import { Provider } from 'react-redux'
import Layout from '@/components/Layout'
import '@/less/index.less'

export default class extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}
    let _initialReduxState = {}
    if (Component.getInitialProps) {
      const { initialReduxState, ..._pageProps } = await Component.getInitialProps(ctx)
      pageProps = {
        ..._pageProps,
        isServer: typeof window === 'undefined',
      }
      _initialReduxState = initialReduxState
    }
    const reduxStore = getOrCreateStore(_initialReduxState)
    ctx.reduxStore = reduxStore

    return {
      pageProps,
      initialReduxState: reduxStore.getState(),
    }
  }

  reduxStore: any

  constructor(props) {
    super(props)
    this.reduxStore = getOrCreateStore(props.initialReduxState)
  }

  render() {
    const { Component, pageProps } = this.props
    return (
      <Container>
        <Head>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no"
          />
        </Head>
        <Provider store={this.reduxStore}>
          <Layout {...pageProps}>
            <Component {...pageProps} />
          </Layout>
        </Provider>
      </Container>
    )
  }
}
